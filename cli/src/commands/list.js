// @flow
const path = require("path");
const chalk = require("chalk");
const process = require("process");
var request = require("request");

const handleErrors = require("../utils/handleErrors");

module.exports.command = "list [number]";
module.exports.describe = "Lists the membership list local the specified VM.";

module.exports.builder = (yargs: any) => yargs;

const machineToIps = {
  "0": "172.16.138.158",
  "1": "172.22.156.15",
  "2": "172.22.158.15",
  "3": "172.22.154.16",
  "4": "172.22.156.16",
  "5": "172.22.158.16",
  "6": "172.22.154.17",
  "8": "172.22.158.17",
  "9": "172.22.154.18",
  "10": "172.22.156.18"
};


module.exports.handler = handleErrors(async (argv: {}) => {
  var target = "http://" + machineToIps[argv.number] +":3000/list/" + argv.number;
  console.log(target)
  request.post(target, function(error, response, body) {
        console.log(body)
        process.exit(0)
    })
});
