// @flow
const path = require("path");
const chalk = require("chalk");
const fs = require("fs");
const util = require("util");
const net = require("net");
const process = require("process");

var express = require("express");
var http = require("http");
var request = require("request");

var app = express();

app.post("/upload/:filename/", function(req, res) {
    console.log("GETTING THIS")
    var filename = path.basename(req.params.filename);
    filename = path.resolve(process.cwd() + "/", filename);
  
    var dst = fs.createWriteStream(filename);
    req.pipe(dst);
    dst.on("drain", function() {
      console.log("drain", new Date());
      req.resume();
    });
    req.on("end", function() {
      res.sendStatus(200);
      process.exit(0);
    });


  
});

app.get('/', (req, res) => res.send('Hello World!'))

const handleErrors = require("../utils/handleErrors");


module.exports.command = "get [sdfsfilename] [localfilename]";
module.exports.describe = "Reads sdfsfilename from the SDFS to localfilename.";
module.exports.builder = (yargs: any) => yargs;

const HOST = "172.16.142.153";
const PORT = 3000;

module.exports.handler = handleErrors(async (argv: {}) => {
  http.createServer(app).listen(3000, "172.16.244.74", function() {
    console.log("Express server listening on port 3000");
  });

  var target = "http://172.22.158.15:3000/get/" + argv.sdfsfilename +  "/" + argv.localfilename;
  var ws = request.post(target);
});
