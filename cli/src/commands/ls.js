// @flow
const path = require("path");
const chalk = require("chalk");
const fs = require("fs");
const util = require("util");
const net = require("net");
const process = require("process");
var request = require("request");

const handleErrors = require("../utils/handleErrors");


module.exports.command = "ls [sdfsfilename]";
module.exports.describe = "Lists all the VMs that currently store sdfsfilename.";
module.exports.builder = (yargs: any) => yargs;

module.exports.handler = handleErrors(async (argv: {}) => {

  var target = "http://172.22.158.15:3000/ls/" + argv.sdfsfilename;
  request.post(target, function(error, response, body) {
        console.log("\The file \"" + argv.sdfsfilename+ "\" is on VMs " + body);
        process.exit(0)
    })
    
});
