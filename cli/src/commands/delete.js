// @flow
const path = require("path");
const chalk = require("chalk");
const fs = require("fs");
const util = require("util");
const net = require("net");
const process = require("process");
var request = require("request");

const handleErrors = require("../utils/handleErrors");


module.exports.command = "delete [sdfsfilename]";
module.exports.describe = "Removes sdfsfilename and all it’s versions from the SDFS.";
module.exports.builder = (yargs: any) => yargs;

module.exports.handler = handleErrors(async (argv: {}) => {
 
  var target = "http://172.22.158.15:3000/delete/" + argv.sdfsfilename;
  request.post(target);

});
