// @flow
const path = require("path");
const chalk = require("chalk");
const fs = require("fs");
const util = require("util");
const net = require("net");
const process = require("process");
var request = require("request");

const handleErrors = require("../utils/handleErrors");


module.exports.command = "put [localfilename] [sdfsfilename]";
module.exports.describe = "Writes localfilename to the SDFS as sdfsfilename.";
module.exports.builder = (yargs: any) => yargs;

module.exports.handler = handleErrors(async (argv: {}) => {
  var target = "http://172.22.158.15:3000/upload/" + argv.sdfsfilename;
  var rs = fs.createReadStream(argv.localfilename);
  var ws = request.post(target);

  ws.on("drain", function() {
    console.log("drain", new Date());
    rs.resume();
  });

  rs.on("end", function() {
    console.log("Uploaded " + argv.localfilename + " as " + argv.sdfsfilename);
    process.exit(0);
  });

  ws.on("error", function(err) {
    console.error("cannot send file to " + target + ": " + err);
  });

  rs.pipe(ws);
});
